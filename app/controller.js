'use strict';

angular.module('Authentication')

    .controller('LoginController',
        ['$scope', '$rootScope', '$location', 'AuthenticationService', '$cookieStore',
            function ($scope, $rootScope, $location, AuthenticationService, $cookieStore) {
                // reset login status
                $scope.login = function () {
                    $scope.dataLoading = true;
                    AuthenticationService.Login($scope.username, $scope.password, function (response) {
                        if (response.success) {
                            AuthenticationService.Login(response.JWT, function (response) {
                                $rootScope.globals = {
                                    currentUser: response
                                };
                                $cookieStore.put('user', response);
                                $location.path('/');
                            });
                        } else {
                            $scope.error = response.message;
                            $scope.dataLoading = false;
                        }
                    });
                };
            }]);