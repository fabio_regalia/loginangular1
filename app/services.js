'use strict';
/*
* service for autentication
*/
angular.module('Authentication')

    .factory('AuthenticationService',
        ['$http', '$cookieStore', '$rootScope', '$timeout',
            function ($http, $cookieStore, $rootScope, $timeout) {
                var service = {};

                service.Login = function (email, password, callback) {
                    //call the server to get the token with username e password
                    $http.post('https://jwt-demo.openshift.techgap.it/login', { username: email, password: password })
                        .success(function (response) {
                            callback(response);
                        }).error(function (error) {
                            callback(error)
                        });

                };
                // call the server with the token to get  profile data
                service.SetData = function (token, callback) {
                    $http.post('https://jwt-demo.openshift.techgap.it/profile', { JWT: token })
                        .success(function (response) {
                            callback(response);
                        }).error(function (error) {
                            callback(error)
                        })
                };

                return service;
            }])
