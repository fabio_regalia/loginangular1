'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Home', []);

angular.module('BasicHttpAuthExample', [
    'Authentication',
    'Home',
    'ngRoute',
    'ngCookies'
])
 
.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'build/template/login.html',
            hideMenus: true
        })
 
        .when('/', {
            controller: 'HomeController',
            templateUrl: 'build/template/home.html'
        })
 
        .otherwise({ redirectTo: '/login' });
}])
 
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('user') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser; // jshint ignore:line
        }
 
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);;'use strict';

angular.module('Authentication')

    .controller('LoginController',
        ['$scope', '$rootScope', '$location', 'AuthenticationService', '$cookieStore',
            function ($scope, $rootScope, $location, AuthenticationService, $cookieStore) {
                // reset login status
                $scope.login = function () {
                    $scope.dataLoading = true;
                    AuthenticationService.Login($scope.username, $scope.password, function (response) {
                        if (response.success) {
                            AuthenticationService.Login(response.JWT, function (response) {
                                $rootScope.globals = {
                                    currentUser: response
                                };
                                $cookieStore.put('user', response);
                                $location.path('/');
                            });
                        } else {
                            $scope.error = response.message;
                            $scope.dataLoading = false;
                        }
                    });
                };
            }]);;'use strict';
 
angular.module('Home')
 
.controller('HomeController',
    ['$scope', '$cookieStore',
    function ($cookieStore) {
      //set variables from the cookie
    }]);;'use strict';

angular.module('Authentication')

    .factory('AuthenticationService',
        ['$http', '$cookieStore', '$rootScope', '$timeout',
            function ($http, $cookieStore, $rootScope, $timeout) {
                var service = {};

                service.Login = function (email, password, callback) {

                    $http.post('https://jwt-demo.openshift.techgap.it/login', { username: email, password: password })
                        .success(function (response) {
                            callback(response);
                        }).error(function (error) {
                            callback(error)
                        });

                };
                service.SetData = function (token, callback) {
                    $http.post('https://jwt-demo.openshift.techgap.it/profile', { JWT: token })
                        .success(function (response) {
                            callback(response);
                        }).error(function (error) {
                            callback(error)
                        })
                };

                return service;
            }])
