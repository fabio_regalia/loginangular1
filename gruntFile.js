module.exports = function (grunt) {
    grunt.initConfig({
        distFolder: 'build',
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            assets: {
                files: [
                    {dest: '<%= distFolder %>/template/', src: '**', expand: true, cwd: 'app/template/'},
                ]
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['app/index.js','app/*.js'],
                dest: '<%= distFolder %>/app/main.js'
            }
        }
    });
    //grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Register our own custom task alias.
    grunt.registerTask('build', ['copy', 'concat']);
};